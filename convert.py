import json
import sys
import os

def main(args):
    filepath = args[1]
    ann = json.load(open(filepath))
    basepath = os.path.basedir(filepath)
    out = []
    for a in ann:
        el = {}
        el['data'] = {'audio': 'http://localhost:8081/'+a['audio_segment_path']}
        el['annotations'] = [{'result':[{
                                        'value': {'text':[a['text']]},
                                        "from_name": "transcription",
                                        "to_name": "audio",
                                        "type": "textarea",
                                        }]}]
        el['predictions'] = [{'score': a['wer'],
                              'model_version': 'version 0',
                              'result':[{
                                        'value': {'text':[a['text_asr']]},
                                        "from_name": "transcription",
                                        "to_name": "audio",
                                        "type": "textarea",
                                        }]}]

        out.append(el)
    fileout = filepath.replace('.json', '_ls.json')
    with open(fileout, 'w') as outf:
        json.dump(out, outf, indent=2)

if __name__ == "__main__":
    main(sys.argv)
