import jiwer
import difflib
from math import isclose
from transformers import pipeline
import re
import os

class Comparator():
    """A class that holds two dicts of segments to calculate WER.
    """

    def __init__(self, segments, model, base_path):
        self.segments = segments
        self.base_path = base_path
        self.asr_pipeline = pipeline('automatic-speech-recognition', model="/home/baybars/repositories/wav2vec2-xls-r-300m-ca-lm")

    def decode(self):
        for segment in self.segments:
            segment_path = os.path.join(self.base_path, segment['audio_segment_path'])
            segment["text_asr"] = self.asr_pipeline(segment_path).get("text")

    def get_wer(self):
        transformation = jiwer.Compose([
            jiwer.ToLowerCase(),
            jiwer.RemoveWhiteSpace(replace_by_space=True),
            jiwer.RemoveMultipleSpaces(),
            jiwer.ReduceToListOfListOfWords(word_delimiter=" ")
        ]) 

        # individual results for each segment
        for segment in self.segments:
            print(segment)
            wer = jiwer.wer(normalize(segment['text']),
                            normalize(segment['text_asr']),
                            truth_transform=transformation, 
                            hypothesis_transform=transformation)
            print(wer, segment['segment_id'])
            segment['wer'] = wer
            if wer != 0.0:
                result = ''
                for item in difflib.ndiff(normalize(segment['text']),
                                          normalize(segment['text_asr'])):
                    result += item
                    segment['diff'] = result

    
def normalize(string):
    accepted_chars = "[^ aàbcçdeéèfghiíïjklmnoóòpqrstuúüvwxyz'0-9·-]"
    pre_clean = re.sub('^\-','',string.replace('\n-',' ')).replace('\n',' ')
    pre_clean = re.sub(' {2,}', ' ', pre_clean)
    return re.sub(accepted_chars, '', pre_clean.lower())
