import torch
import numpy as np
import soundfile as sf
from typing import List
import ctc_segmentation
from datasets import load_dataset
from transformers import Wav2Vec2ProcessorWithLM, Wav2Vec2ForCTC, Wav2Vec2CTCTokenizer

class Aligner():
    """Uses HF wav2vec2 to do alignment operations
    """

    def __init__(self, model):
        #model_name = '/home/baybars/repositories/wav2vec2-xls-r-300m-ca-lm'
        self.processor = Wav2Vec2ProcessorWithLM.from_pretrained(model)
        self.tokenizer = Wav2Vec2CTCTokenizer.from_pretrained(model)
        self.model = Wav2Vec2ForCTC.from_pretrained(model)

        # load dummy dataset and read soundfiles
        self.sample_rate = 16000

    def align_with_text(
        self,
        filename : str,
        transcripts : List[str],
    ):
        audio, sr = sf.read(filename)
        assert audio.ndim == 1
        # Run prediction, get logits and probabilities
        inputs = self.processor(audio, return_tensors="pt",
                                padding="longest", sampling_rate=self.sample_rate)
        with torch.no_grad():
            logits = self.model(inputs.input_values).logits.cpu()[0]
            probs = torch.nn.functional.softmax(logits,dim=-1)

        # Tokenize transcripts
        vocab = self.tokenizer.get_vocab()
        inv_vocab = {v:k for k,v in vocab.items()}
        unk_id = vocab["[UNK]"]
        
        tokens = []
        for transcript in transcripts:
            assert len(transcript) > 0
            tok_ids = self.tokenizer(transcript.replace("\n"," ").lower())['input_ids']
            tok_ids = np.array(tok_ids,dtype=int)
            tokens.append(tok_ids[tok_ids != unk_id])
        
        # Align
        char_list = [inv_vocab[i] for i in range(len(inv_vocab))]
        config = ctc_segmentation.CtcSegmentationParameters(char_list=char_list)
        config.index_duration = audio.shape[0] / probs.size()[0] / self.sample_rate
        ground_truth_mat, utt_begin_indices = ctc_segmentation.prepare_token_list(config,
                                                                                  tokens)
        timings, char_probs, state_list = ctc_segmentation.ctc_segmentation(config,
                                                                            probs.numpy(),
                                                                            ground_truth_mat)
        segments = ctc_segmentation.determine_utterance_segments(config, utt_begin_indices, char_probs, timings, transcripts)
        return [{"aligner_text" : t,
                 "aligner_start" : p[0],
                 "aligner_end" : p[1],
                 "conf" : p[2]} for t,p in zip(transcripts, segments)]

def get_overlap(segments):
    ref = segments['m47']
    tar = segments['pangeanic']
    eps = 1
    for task in ref.keys():
        for r_segment in ref[task]:
            for t_segment in tar[task]:
                if (t_segment['start'] > r_segment['start']-eps and \
                    t_segment['start'] < r_segment['end']+eps) or \
                   (t_segment['end'] > r_segment['start']-eps and \
                    t_segment['end'] < r_segment['end']+eps) or \
                   (t_segment['start'] < r_segment['start'] and
                    t_segment['end'] > r_segment['end']):
                    t_segment['overlap'] = True
