# Evaluate annotations

This repository evaluates the results of the annotations of the providers, starting from the delivery json, extracting the transcription, generating `wer` and `deviation`; and converting it to label-studio input format.

## Installation

Requirement file not prepared yet

## Launch

```
python evaluate-annotate.py <path_to_json>
```

for now `convert.py` has to be launched to convert script results to label-studio format.

## Todo

[] add requirements and test
[] merge convert.py with the main script
[] change speech recognition pipeline
