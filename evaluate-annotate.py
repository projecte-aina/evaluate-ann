import sys
import os
import json
import re
from align_hf import Aligner, get_overlap
from wer import Comparator

MODEL = "PereLluis13/wav2vec2-xls-r-300m-ca"

def main(args):
    filename = args[1]
    print(os.path.dirname(filename))
    base_path = os.path.join(os.path.dirname(filename), '..')
    segments = get_segments(filename)

    eval_time(segments, base_path)
    eval_wer(segments, base_path)

    with open('results_test.json', 'w') as out:
        json.dump(segments, out, indent=2)

def get_segments(filename):
    return json.load(open(filename))

def eval_time(segments, base_path):
    aligner = Aligner(MODEL)
    total_dev = 0
    segment_path_key = 'audio_segment_path'
    text_key = 'text'

    for segment in segments:
        segment['start'] = convert_time_stamp(segment['start'])
        segment['end'] = convert_time_stamp(segment['end'])
        if segment['end'] - segment['start'] > 0:
            segment_path = os.path.join(base_path, segment[segment_path_key])
            result = aligner.align_with_text(segment_path,
                                             [segment[text_key]])
            segment.update(result[0])
            eval_av_deviation(segment)
            print(segment['deviation'], segment["audio_segment_path"])
        else:
            print('negative duration:', segment['start'], segment['end'])

def convert_time_stamp(time_stamp):
    time_stamp = re.sub(r',', '.', time_stamp)
    time_stamp = time_stamp.split(':')
    time_stamp = [float(i) for i in time_stamp]
    time_stamp = time_stamp[0]*3600 + time_stamp[1]*60 + time_stamp[2]
    return time_stamp

def eval_av_deviation(segment):
    #print(json.dumps(segment, indent=2))
    duration = segment['end'] - segment['start']
    segment['deviation'] = segment['aligner_start']+\
                           abs((duration-segment['aligner_end']))/2
    segment['duration'] = duration

def eval_wer(segments, base_path):
    comparator = Comparator(segments, MODEL, base_path)
    comparator.decode()
    comparator.get_wer()
    segments = comparator.segments

if __name__ == "__main__":
    main(sys.argv)
